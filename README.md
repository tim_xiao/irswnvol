# irSwnVol

An interest rate swaption volatility surface is a four-dimensional plot of the implied volatility of a swaption as a function of strike and expiry and tenor. 